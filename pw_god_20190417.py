# DISCRIPTION: Test analytical model of polar wind at Saturn from equations in Glocer+ 2007
# AUTHOR: C.J.Martin
# INSTITUTION: Lancaster University

# INPUTS: none other than the usual

# OUTPUT: Plots for now

# NOTES: two vectors for each variable are used, a long version used for 
# calculting the spatial steps and a short version '_s' used for calculating
# the final results for each iteration.
# Current inputs are from Moore+ 2008 for SATURN
# All in SI units unless explicitly commented

# HISTORY:
# 20190304 - Created
# 20190318 - Iterative Version
# 20190410 - Implicit calculation of dT/dr - predictor-corrector


#==============================================================================
# # ----------------Imports-------------------
#==============================================================================
import numpy as np
import matplotlib.pyplot as pl


#==============================================================================
# #------------------functions----------------
#==============================================================================
#Function to calculate the reduced mass of two species SCHUNK + NAGY 2000
def red_mass(m_i,m_j): 
    return (m_i*m_j)/(m_i+m_j)
    
#Function to calculate the collision frequency between two species- SCHUNK + NAGY 2000
def collision_freq(rho_n,m_i,m_j,lambda_n, e_charge):
    return 2.21*np.pi*(rho_n/(m_i+m_j))*((lambda_n*e_charge**2)/\
                       red_mass(m_i,m_j))**0.5

#Function to calculate the momentum exhange rate for one species with one other
# species SCHUNK + NAGY 2000
def momentum_rate_1species(rho_n,m_i,m_j,lambda_n, e_charge,rho_i,u_i):
    return rho_i*collision_freq(rho_n,m_i,m_j,lambda_n, e_charge)*u_i

#Function to calculate the energy exchange rate for one ion with one neutral SCHUNK + NAGY 2000
def energy_rate_1species(rho_i,rho_n,m_i,m_j,lambda_n, e_charge,T_j,T_i,u_i,k_b):
    return ((rho_i * collision_freq(rho_n,m_i,m_j,lambda_n, e_charge))/\
            (m_i + m_j)) * (3* k_b * (T_j-T_i) + m_j*u_i**2)

#Function to calculate heat conductivity using assumptions in Banks+Kockarts 
#1973, Moore+ 2008 
def heat_conductivity(T,e_charge,m_i,m_p):
    return (4.6*10**4 *(m_i/m_p)**-0.5 * T**2.5)*e_charge*100 #J m-1 s-1 K-1

# from Raitt+ 1975: 7.7 x 10^5   new one is from Banks + Kockharts
def heat_conductivity_electrons(T,e_charge,gamma):
    return ((1.8*10**6)*(T**(5/2)))*e_charge*100 #J m-1 s-1 K-1

#Function to calculate plasma pressure
def plasma_pressure(n,k_b,T):
    return (n * k_b * T)

def plasma_temperature(n,k_b,P):
    return (P/(n*k_b))

#Function to calculate second term in electric field with 2 ionic species
def E_second_term(m_e,m_1,m_2,u_e,u_1,u_2,S_1,S_2,dMdt_1,dMdt_2):
    return (m_e/m_1 * ((u_e-u_1)*S_1-dMdt_1)) + (m_e/m_2 *\
           ((u_e-u_2)*S_2-dMdt_2))

#Function to calculate electric field
def E_parallel_short(e_charge, n_e, dFdt,A, dAdr, rho_e,u_e):
    return (-1/(e_charge*n_e) * (dFdt + (dAdr/A)*rho_e*u_e**2))

#Personalised plotting function by me for me
def plot_me_quick(x,y,xlabel,ylabel,grid):
    pl.plot(x,y)
    pl.ylabel(ylabel)
    pl.xlabel(xlabel)
    pl.grid(grid) 
    pl.xlim([0,62000])

def density_dt_ion(dt,A,S,rho,dFdt):
    return ((dt/A)*(A*S-dFdt)+rho)
 
def density_dt_electron(rho_1,rho_2,m_1,m_2,me):
    return ((rho_1/m_1 + rho_2/m_2)*m_e) #(rho_H_plus_dt/m_H_plus + rho_H3_plus_dt/m_H3_plus)
    #*m_e 

def velocity_dt_ion(dt,A,rho,rho_1,dFdr,dPdr,m_i,E,e_charge,g,dMdt,u,S):
    return ((dt/(A*rho_1))*(-dFdr - (A*dPdr) +(A*rho*((e_charge/m_i)*E - g)) + \
           A*dMdt + A*u*S)+((rho*u)/rho_1))

def velocity_dt_electron(n_e,n_1,n_2,u_1,u_2):
    return (1/n_e * (n_1*u_1 + n_2*u_2)) #this is where field aligned currents 
    #would go IF I HAD ANY
    
def pressure_dt_ion(dt,A,gamma,rho,rho_1,u,u_1,m_i, e_charge,E,g,dEdt,dMdt,P,dEngdr,dakTdr,S):    
#    return (((dt/A)*(gamma-1))*(-dEngdr+A*rho*u*((e_charge/m_i)*E-g)+A*dEdt+\
#     A*u*dMdt+0.5*A*u**2*S)+(0.5*rho*u**2 *(gamma-1))+P-(0.5*rho_1*u_1**2*(gamma-1)))
    return (((dt/A)*(gamma-1))*(-dEngdr+A*rho*u*((e_charge/m_i)*E-g)+dakTdr+A*dEdt+\
     A*u*dMdt+0.5*A*u**2*S)+(0.5*rho*u**2 *(gamma-1))+P-(0.5*rho_1*u_1**2*(gamma-1)))
     
def temperature_dt_electron(dt,gamma,m_e,k_b,A,rho,u,T,S,dTedr,dAudr,dakTedr):
    return (dt*((gamma-1)*(m_e/(k_b*A*rho))*dakTedr-u*dTedr-(T/rho)*(S+((gamma-1)/A)*rho*dAudr))+T)

def electron_flux(n_i1,u_i1,n_i2,u_i2):
    return ((n_i1*u_i1)+(n_i2*u_i2))

def eV2vel(eV, m):
    return np.sqrt((eV*2*1.6*10**-19)/(m))

def vel2eV(vel, m):
    return (m*vel**2)/(2*1.6*10**-19)

def v2T(v,m):
    return (m*v**2)/(2*1.38*10**-23)

def T2v(T,m):
    return np.sqrt((2*1.38*10**-23*T)/m)

def extrap_end(a):
    return [2*a[-1]-a[-2],2*(2*a[-1]-a[-2])-a[-1]]

def extrap_start(a):
    return [2*(2*a[0]-a[1])-a[0],2*a[0]-a[1]]

#def best_fit_poly(z,a,z_ext):
#    mean_error= np.empty(12,)
#    for j in range(3,15):
#        p = np.polyfit(z, a[2:-2], j)
#        y = np.polyval(p, z_ext)
#        mean_error[j-3] = (1/len(y)) * np.nansum((a-y)**2)
#    min_poly=np.nanargmin(mean_error)
#    p = np.polyfit(z, a[2:-2], min_poly+3)
#    return np.polyval(p, z_ext)
#
#def best_fit_poly_vals(z,a,z_ext):
#    mean_error= np.empty(8,)
#    for j in range(2,10):
#        p = np.polyfit(z, a[2:-2], j)
#        y = np.polyval(p, z_ext)
#        mean_error[j-3] = (1/len(y)) * np.nansum((a-y)**2)
#    min_poly=np.nanargmin(mean_error)
#    p = np.polyfit(z, a[2:-2], min_poly+3)
#    return p
#
#def second_diff_function_fit(z,z_ext,T):
#    coords = best_fit_poly_vals(z,T,z_ext)
#    first_diff = np.polyder(coords)
#    second_diff= np.polyder(first_diff)
#    return np.polyval(second_diff,z_ext)

#==============================================================================
# #--------------Start main-------------------
#==============================================================================
#debugging plots yes=1/no=0
dbug = 1
if dbug ==1:
    pl.figure(1)

#set up grid
z = np.linspace(1400000,61325000,800) #meters
z_ext = np.linspace(1250000,61475000,804)
x = np.linspace(0,799,800)
dz = 75000.0

#ghost points
gb_z = np.linspace(1250000,1325000,2)
ge_z = np.linspace(61400000,61475000,2)
gb_x = np.linspace(-1,2,2)
ge_x = np.linspace(800,801,2)

#time steps
dt = 0.1 #seconds

#iterations
its = 2


#prefill fized arrays
A = np.empty([len(z)+4,])
n_H2 = np.empty([len(z)+4,])
rho_H2 = np.empty([len(z)+4,])
n_He = np.empty([len(z)+4,])
rho_He = np.empty([len(z)+4,])
S_H_plus = np.empty([len(z)+4,])
S_H3_plus = np.empty([len(z)+4,])
S_e = np.empty([len(z)+4,])
T_neut = np.empty([len(z)+4,])
P_neut = np.empty([len(z)+4,])
# prefill iterative arrays
E = np.empty([len(z)+4,its])
E[:,0] = np.nan # no initial values for electric field
e_flux = np.empty([len(z)+4,its])
e_flux[:,0] = np.nan # no initial values for electron flux
H_plus_flux = np.empty([len(z)+4,its])
H_plus_flux[:,0] = np.nan # no initial values for electron flux
H3_plus_flux = np.empty([len(z)+4,its])
H3_plus_flux[:,0] = np.nan # no initial values for electron flux
rho_H_plus = np.empty([len(z)+4,its])
rho_H3_plus = np.empty([len(z)+4,its])
rho_e = np.empty([len(z)+4,its])
n_H_plus = np.empty([len(z)+4,its])
n_H3_plus = np.empty([len(z)+4,its])
n_e = np.empty([len(z)+4,its])
u_H_plus = np.empty([len(z)+4,its])
u_H3_plus = np.empty([len(z)+4,its])
u_e = np.empty([len(z)+4,its])
P_H_plus = np.empty([len(z)+4,its])
P_H3_plus = np.empty([len(z)+4,its])
P_e = np.empty([len(z)+4,its])
T_H_plus = np.empty([len(z)+4,its])
T_H3_plus = np.empty([len(z)+4,its])
T_e = np.empty([len(z)+4,its])
kappa_H_plus = np.empty([len(z)+4,its])
kappa_H3_plus = np.empty([len(z)+4,its])
kappa_e = np.empty([len(z)+4,its])


# Constants set-up
e_charge = 1.60217662*10**-19  #C - electron charge
m_p = 1.6726*10**-27 # kg - mass of a proton
m_e = 9.10938356 * 10**-31 # kg - mass of an electron
k_b = 1.38064852*10**-23 #m2 kg s-2 K-1  - boltzmann constant
lambda_H_plus = 1.66 #spcific heat ratio for H+ 
#lambda_H3_plus =   #spcific heat ratio for H3+ 
g = 10.44 #graviational acceleration
m_H2 = 2*m_p
m_He = 4*m_p
m_H_plus = m_p
m_H3_plus = 3*m_p
#Neutral gas polarizabilities
lambda_H2 = 0.82*10**-30 # m^3 from 0.82*10**-24 cm^3 Schunk&Nagy 2000
lambda_He = 0.21*10**-30 # m^3 from 0.21*10**-24 cm^3 Schunk&Nagy 2000
gamma = 5/3 #specific heat ratio



# cross sectional area A
alp= (1/(z[0]**3)) *0.00000001 
A[2:-2] = alp*(z**3)
A[0:2]=alp*(gb_z**3)
A[-2:]=alp*(ge_z**3)

if dbug == 1:
    pl.subplot(4,2,1)
    pl.plot(z/1000,A[2:-2])
    pl.ylabel('Cross-sectional Area - A (m^2)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([0,0.001])
    pl.xlim([0,62000])
    pl.grid('on')    
    
#centre position of cylindrical box
z_c = np.linspace(1462500,61400000,801) 
    
# half step cross-sectional area
A_c = alp*(z_c**3)



# cross section radius - meters
R_c = A_c/ (2*np.pi)

#box volumes - meters cubed
V = np.empty([len(A),])
for ii in range(0,len(R_c)-1):
    V[ii] = (1/3 * np.pi * dz *(R_c[ii]**2 + R_c[ii]*R_c[ii+1] + R_c[ii+1]**2))     
    
#==============================================================================
# # -----------------------Initial Profiles------------------------------------   
#==============================================================================
# Each profile has 2 ghost points on either end 
#Initial H+ temperature profile and hence pressure
T_H_plus[2:-2,0] = 25* (x/20)**2*np.exp(-0.1*(x/20)) +400
T_H_plus[0,0]=400
T_H_plus[1,0]=400
T_H_plus[-2:,0]=25* (ge_x/20)**2*np.exp(-0.1*(ge_x/20)) +400

#Initial H3+ temperature profile and hence pressure
T_H3_plus[2:-2,0] = 30* (x/15)**2*np.exp(-0.1*(x/15)) +400
T_H3_plus[0,0]=400
T_H3_plus[1,0]=400
T_H3_plus[-2:,0]=30* (ge_x/15)**2*np.exp(-0.1*(ge_x/15)) +400

# initial H+ ion velocity - 1eV proton eV2vel(1,m_H_plus)
u_H_plus[2:-2,0] = T2v(T_H_plus[2:-2,0],m_H_plus)
u_H_plus[0,0]=T2v(T_H_plus[0,0],m_H_plus)
u_H_plus[1,0]=T2v(T_H_plus[1,0],m_H_plus)
u_H_plus[-1,0]=T2v(T_H_plus[-1,0],m_H_plus)
u_H_plus[-2,0]=T2v(T_H_plus[-2,0],m_H_plus)
if dbug==1:
    pl.subplot(4,2,2)
    line1,=pl.plot(z/1000,u_H_plus[2:-2,0])
    pl.ylabel('Initial Velocity (m/s)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,1])
    pl.xlim([0,62000])
    pl.grid('on')
    
# initial H3+ ion velocity - 1eV ion
u_H3_plus[2:-2,0] = T2v(T_H3_plus[2:-2,0],m_H3_plus)
u_H3_plus[0,0]=T2v(T_H3_plus[0,0],m_H3_plus)
u_H3_plus[1,0]=T2v(T_H3_plus[1,0],m_H3_plus)
u_H3_plus[-1,0]=T2v(T_H3_plus[-1,0],m_H3_plus)
u_H3_plus[-2,0]=T2v(T_H3_plus[-2,0],m_H3_plus)
if dbug==1:
    pl.subplot(4,2,2)
    line2,=pl.plot(z/1000,u_H3_plus[2:-2,0])
    pl.grid('on')
    

#initial H+ ion density - exponential decrease place holder
n_H_plus[2:-2,0] = 2*10**10*np.exp(-0.5*(z/z[0])) + 6*10**5
n_H_plus[0:2,0]=2*10**10*np.exp(-0.5*(gb_z/z[0])) + 6*10**5
n_H_plus[-2:,0]=2*10**10*np.exp(-0.5*(ge_z/z[0])) + 6*10**5
rho_H_plus[2:-2,0] = n_H_plus[2:-2,0] * m_H_plus
rho_H_plus[0:2,0]=n_H_plus[0:2,0] * m_H_plus
rho_H_plus[-2:,0]=n_H_plus[-2:,0] * m_H_plus
if dbug==1:
    pl.subplot(4,2,3)
    n1,=pl.plot(z/1000,n_H_plus[2:-2,0])
    pl.subplot(4,2,4)
    rho1,=pl.plot(z/1000,rho_H_plus[2:-2,0])
    
#initial H3+ ion density - exponential decrease place holder
n_H3_plus[2:-2,0] = 5*10**9*np.exp(-0.4*(z/z[0])) + 10**5
n_H3_plus[0:2,0]=5*10**9*np.exp(-0.4*(gb_z/z[0])) + 10**5
n_H3_plus[-2:,0]=5*10**9*np.exp(-0.4*(ge_z/z[0])) + 10**5
rho_H3_plus[2:-2,0] = n_H3_plus[2:-2,0] * m_H3_plus
rho_H3_plus[0:2,0]=n_H3_plus[0:2,0] * m_H3_plus
rho_H3_plus[-2:,0]=n_H3_plus[-2:,0] * m_H3_plus
if dbug==1:
    pl.subplot(4,2,3)
    n2,=pl.plot(z/1000,n_H3_plus[2:-2,0])
    pl.ylabel('Initial Number Density (m^-3)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,10])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.subplot(4,2,4)
    rho2,=pl.plot(z/1000,rho_H3_plus[2:-2,0])
    pl.ylabel('Initial Mass Density (m^-3)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,10])
    pl.xlim([0,62000])
    pl.grid('on') 
    
#initial electron density - quasi-neutrality
n_e[2:-2,0] = n_H3_plus[2:-2,0]+n_H_plus[2:-2,0]
n_e[0:2,0]=n_H3_plus[0:2,0]+n_H_plus[0:2,0]
n_e[-2:,0]=n_H3_plus[-2:,0]+n_H_plus[-2:,0]
rho_e[2:-2,0] = n_e[2:-2,0] * m_e
rho_e[0:2,0]=n_e[0:2,0]* m_e
rho_e[-2:,0]=n_e[-2:,0]* m_e

if dbug==1:
    pl.subplot(4,2,3)
    n3,=pl.plot(z/1000,n_e[2:-2,0])
    pl.subplot(4,2,4)
    rho3,=pl.plot(z/1000,rho_e[2:-2,0])    
    
# initial electron velocity - calcualted from ion velocities and densities - quasinauetrality
u_e[2:-2,0] = (1/n_e[2:-2,0]) * (n_H3_plus[2:-2,0] * u_H3_plus[2:-2,0] + n_H_plus[2:-2,0]*u_H_plus[2:-2,0])
u_e[0:2,0]=(1/n_e[0:2,0]) * (n_H3_plus[0:2,0] * u_H3_plus[0:2,0] + n_H_plus[0:2,0]*u_H_plus[0:2,0])
u_e[-2:,0]=(1/n_e[-2:,0]) * (n_H3_plus[-2:,0] * u_H3_plus[-2:,0] + n_H_plus[-2:,0]*u_H_plus[-2:,0])
if dbug==1:
    pl.subplot(4,2,2)
    line3,=pl.plot(z/1000,u_e[2:-2,0])
    pl.legend((line1, line2, line3), ('H+', 'H3+', 'e -'))
  
#initial neutral H2 density - CONSTANT
n_H2[2:-2] = (10**10)*10**6*np.exp(-0.3*(z/z[0])) + 10000000
n_H2[0:2]=(10**10)*10**6*np.exp(-0.3*(gb_z/z[0])) + 10000000
n_H2[-2:]=(10**10)*10**6*np.exp(-0.3*(ge_z/z[0])) + 10000000
rho_H2[2:-2] = n_H2[2:-2] * m_H2
rho_H2[0:2]=n_H2[0:2] * m_H2
rho_H2[-2:]=n_H2[-2:] * m_H2

if dbug==1:
    pl.subplot(4,2,3)
    n4,=pl.plot(z/1000,n_H2[2:-2])
    pl.subplot(4,2,4)
    rho4,=pl.plot(z/1000,rho_H2[2:-2])
 
#initial neutral He density - CONSTANT
n_He[2:-2] = 10**5*10**6*np.exp(-0.5*(z/z[0])) + 10000000
n_He[0:2]=10**5*10**6*np.exp(-0.5*(gb_z/z[0])) + 10000000
n_He[-2:]=10**5*10**6*np.exp(-0.5*(ge_z/z[0])) + 10000000
rho_He[2:-2] = n_He[2:-2] * m_He
rho_He[0:2]=n_He[0:2] * m_He
rho_He[-2:]=n_He[-2:] * m_He
if dbug==1:
    pl.subplot(4,2,3)
    n5,=pl.plot(z/1000,n_He[2:-2])
    pl.legend((n1,n2,n3,n4,n5),('H+','H3+','e -','H2','He'))
    pl.yscale('log')
    pl.subplot(4,2,4)
    rho5,=pl.plot(z/1000,rho_He[2:-2])   
    pl.legend((rho1,rho2,rho3,rho4,rho5),('H+','H3+','e -','H2','He'))
    pl.yscale('log')
 
# H+ mass production rate    - CONSTANT
S_H_plus[2:-2] = 10**8 * m_H_plus *np.exp(-z/10000000) + 10**3* m_H_plus
S_H_plus[0:2]=10**8 * m_H_plus *np.exp(-gb_z/10000000) + 10**3* m_H_plus
S_H_plus[-2:]=10**8 * m_H_plus *np.exp(-ge_z/10000000) + 10**3* m_H_plus

if dbug==1:
    pl.subplot(4,2,5)
    s1,=pl.plot(z/1000,S_H_plus[2:-2])
    pl.ylabel('Mass Production Rate (kg m^-3 s^-1)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([10**-22,10**-17])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.yscale('log')

# H3+ mass production rate      - CONSTANT
S_H3_plus[2:-2] = 10**8 * m_H3_plus *np.exp(-z/10000000) + 10**3* m_H3_plus
S_H3_plus[0:2]=10**8 * m_H3_plus *np.exp(-gb_z/10000000) + 10**3* m_H3_plus
S_H3_plus[-2:]=10**8 * m_H3_plus *np.exp(-ge_z/10000000) + 10**3* m_H3_plus
S_e[2:-2] = (S_H3_plus[2:-2]/m_H3_plus + S_H_plus[2:-2]/m_H_plus) * m_e
S_e[0:2] = (S_H3_plus[0:2]/m_H3_plus + S_H_plus[0:2]/m_H_plus) * m_e
S_e[-2:] = (S_H3_plus[-2:]/m_H3_plus + S_H_plus[-2:]/m_H_plus) * m_e
if dbug==1:
    pl.subplot(4,2,5)
    s2,=pl.plot(z/1000,S_H3_plus[2:-2])
    s3,=pl.plot(z/1000,S_e[2:-2])
    pl.legend((s1,s2,s3),('H+','H3+','e -'))



P_H_plus[2:-2,0] = plasma_pressure(n_H_plus[2:-2,0],k_b,T_H_plus[2:-2,0])
P_H_plus[0:2,0]= plasma_pressure(n_H_plus[0:2,0],k_b,T_H_plus[0:2,0])
P_H_plus[-2:,0]= plasma_pressure(n_H_plus[-2:,0],k_b,T_H_plus[-2:,0])
if dbug==1:
    pl.subplot(4,2,6)
    t1,=pl.plot(z/1000,T_H_plus[2:-2,0])
    pl.ylabel('Temperature (K)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([0,2000])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.subplot(4,2,7)
    p1,=pl.plot(z/1000,P_H_plus[2:-2,0])
    pl.ylabel('Pressure (N/m^2)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([10**-33,10**-4])
    pl.yscale('log')
    pl.xlim([0,62000])
    pl.grid('on')    
    
P_H3_plus[2:-2,0] = plasma_pressure(n_H3_plus[2:-2,0],k_b,T_H3_plus[2:-2,0])
P_H3_plus[0:2,0]= plasma_pressure(n_H3_plus[0:2,0],k_b,T_H3_plus[0:2,0])
P_H3_plus[-2:,0]= plasma_pressure(n_H3_plus[-2:,0],k_b,T_H3_plus[-2:,0])
if dbug==1:
    pl.subplot(4,2,6)
    t2,=pl.plot(z/1000,T_H3_plus[2:-2,0])
    pl.subplot(4,2,7)
    p2,=pl.plot(z/1000,P_H3_plus[2:-2,0])
    
#Initial electron temperature profile and hence pressure 
T_e[2:-2,0] = T_H_plus[2:-2,0] +T_H3_plus[2:-2,0]
T_e[0:2,0]=T_H_plus[0:2,0] +T_H3_plus[0:2,0]
T_e[-2:,0]=T_H_plus[-2:,0] +T_H3_plus[-2:,0]

P_e[2:-2,0] = plasma_pressure(n_e[2:-2,0],k_b,T_e[2:-2,0])
P_e[0:2,0]=plasma_pressure(n_e[0:2,0],k_b,T_e[0:2,0])
P_e[-2:,0]=plasma_pressure(n_e[-2:,0],k_b,T_e[-2:,0])

if dbug==1:
    pl.subplot(4,2,6)
    t3,=pl.plot(z/1000,T_e[2:-2,0])
    pl.subplot(4,2,7)
    p3,=pl.plot(z/1000,P_e[2:-2,0])
    
#Initial neutral temperature profile and hence pressure
T_neut[2:-2] = np.ones(np.size(x))* 400
T_neut[0:2]=np.ones(np.size(gb_x))* 400
T_neut[-2:]=np.ones(np.size(ge_x))* 400
P_neut[2:-2] = (n_H2[2:-2]+n_He[2:-2]) * k_b * T_neut[2:-2]
P_neut[0:2]=(n_H2[0:2]+n_He[0:2]) * k_b * T_neut[0:2]
P_neut[-2:]=(n_H2[-2:]+n_He[-2:]) * k_b * T_neut[-2:]

if dbug==1:
    pl.subplot(4,2,6)
    t4,=pl.plot(z/1000,T_neut[2:-2])
    pl.legend((t1,t2,t3,t4),('H+','H3+','e-','n'))
    pl.subplot(4,2,7)
    p4,=pl.plot(z/1000,P_neut[2:-2])
    pl.legend((p1,p2,p3,p4),('H+','H3+','e-','n'))    
    
# heat conductivities
kappa_H_plus[2:-2,0] = heat_conductivity(T_H_plus[2:-2,0],e_charge,m_H_plus,m_p)
kappa_H_plus[0:2,0]=heat_conductivity(T_H_plus[0:2,0],e_charge,m_H_plus,m_p)
kappa_H_plus[-2:,0]=heat_conductivity(T_H_plus[-2:,0],e_charge,m_H_plus,m_p)
#kappa_H_plus[:,0]= np.ones(np.size([len(z)+4,]))*5.7*10**-5

kappa_H3_plus[2:-2,0] = heat_conductivity(T_H3_plus[2:-2,0],e_charge,m_H3_plus,m_p)
kappa_H3_plus[0:2,0]=heat_conductivity(T_H3_plus[0:2,0],e_charge,m_H3_plus,m_p)
kappa_H3_plus[-2:,0]=heat_conductivity(T_H3_plus[-2:,0],e_charge,m_H3_plus,m_p)
#kappa_H3_plus[:,0]= np.ones(np.size([len(z)+4,]))*3.8*10**-5

kappa_e[2:-2,0] = heat_conductivity_electrons(T_e[2:-2,0],e_charge,gamma)
kappa_e[0:2,0]=heat_conductivity_electrons(T_e[0:2,0],e_charge,gamma)
kappa_e[-2:,0]=heat_conductivity_electrons(T_e[-2:,0],e_charge,gamma)
#kappa_e[:,0]= np.ones(np.size([len(z)+4,]))*7.9*10**-12

if dbug==1:
    pl.subplot(4,2,8)
    k1,=pl.plot(z/1000,kappa_H_plus[2:-2,0])
    k2,=pl.plot(z/1000,kappa_H3_plus[2:-2,0])
    k3,=pl.plot(z/1000,kappa_e[2:-2,0])
    pl.legend((k1,k2,k3),('H+','H3+','e -'))
    pl.ylabel('Heat Conductivity (J m-1 s-1 K-1)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.xlim([0,62000])
    pl.grid('on')     
    pl.subplots_adjust(wspace=0.5, hspace=0.5)  
    
#==============================================================================
#     Begin calculations
#==============================================================================

#differntials
dPdr = (np.roll(P_H_plus[:,0],-1)-np.roll(P_H_plus[:,0],1))/(2*dz)
dPdr[0] = np.nan
dPdr[-1] = np.nan
dAdr = (np.roll(A,-1)-np.roll(A,1))/(2*dz)
dAdr[0] = np.nan
dAdr[-1] = np.nan
dPrhou2 = (np.roll(P_e[:,0]+rho_e[:,0]*u_e[:,0]**2,-1)-np.roll(P_e[:,0]+rho_e[:,0]*u_e[:,0]**2,1))/(2*dz)
dPrhou2[0] = np.nan
dPrhou2[-1] = np.nan
dEdr = np.zeros([len(z_ext),])

#parallel electric field - initial (short assumption)
E[2:-2,1] = E_parallel_short(e_charge, n_e[2:-2,0].T, dPrhou2[2:-2], A[2:-2].T, dAdr[2:-2], rho_e[2:-2,0].T, u_e[2:-2,0].T) + 1/(e_charge*n_e[2:-2,0]) * dEdr[2:-2].T
E[0:2,1]=extrap_start(E[2:-2,1])
E[-2:,1]=extrap_end(E[2:-2,1])

#Calculate left and right slopes
delta_rho_j_minus=np.empty([len(z_ext),])
delta_rho_j_plus=np.empty([len(z_ext),])
rho_limited_slope=np.empty([len(z_ext),])
rho_right_face_j_plus=np.empty([len(z_ext),])
rho_left_face_j_plus=np.empty([len(z_ext),])
rho_right_face_j_minus=np.empty([len(z_ext),])
rho_left_face_j_minus=np.empty([len(z_ext),])
delta_u_j_minus=np.empty([len(z_ext),])
delta_u_j_plus=np.empty([len(z_ext),])
u_limited_slope=np.empty([len(z_ext),])
u_right_face_j_plus=np.empty([len(z_ext),])
u_left_face_j_plus=np.empty([len(z_ext),])
u_right_face_j_minus=np.empty([len(z_ext),])
u_left_face_j_minus=np.empty([len(z_ext),])
delta_p_j_minus=np.empty([len(z_ext),])
delta_p_j_plus=np.empty([len(z_ext),])
p_limited_slope=np.empty([len(z_ext),])
p_right_face_j_plus=np.empty([len(z_ext),])
p_left_face_j_plus=np.empty([len(z_ext),])
p_right_face_j_minus=np.empty([len(z_ext),])
p_left_face_j_minus=np.empty([len(z_ext),])
for j in range(1,len(z_ext)-1):
    delta_rho_j_minus = rho_H_plus[j,0] - rho_H_plus[j-1,0]
    delta_rho_j_plus = rho_H_plus[j+1,0] - rho_H_plus[j,0]
    rho_limited_slope = (delta_rho_j_plus/abs(delta_rho_j_plus)) * np.min([1.5*delta_rho_j_plus,1.5*delta_rho_j_minus,(rho_H_plus[j+1,0]-rho_H_plus[j-1,0])/(2*dz)])  
    rho_right_face_j_plus[j] = rho_H_plus[j,0] + 0.5*rho_limited_slope 
    rho_left_face_j_plus[j] = rho_H_plus[j,0] - 0.5*rho_limited_slope 
    rho_right_face_j_minus[j] = rho_H_plus[j,0] - 0.5*rho_limited_slope 
    rho_left_face_j_minus[j] = rho_H_plus[j-1,0] + 0.5*rho_limited_slope     
    delta_u_j_minus = rho_H_plus[j,0] - rho_H_plus[j-1,0]
    delta_u_j_plus = rho_H_plus[j+1,0] - rho_H_plus[j,0]
    u_limited_slope = (delta_rho_j_plus/abs(delta_rho_j_plus)) * np.min([1.5*delta_rho_j_plus,1.5*delta_rho_j_minus,(rho_H_plus[j+1,0]-rho_H_plus[j-1,0])/(2*dz)])  
    u_right_face_j_plus[j] = u_H_plus[j,0] + 0.5*u_limited_slope 
    u_left_face_j_plus[j] = u_H_plus[j,0] - 0.5*u_limited_slope 
    u_right_face_j_minus[j] = u_H_plus[j,0] - 0.5*u_limited_slope 
    u_left_face_j_minus[j] = u_H_plus[j-1,0] + 0.5*u_limited_slope
    delta_p_j_minus = P_H_plus[j,0] - P_H_plus[j-1,0]
    delta_p_j_plus = P_H_plus[j+1,0] - P_H_plus[j,0]
    p_limited_slope = (delta_p_j_plus/abs(delta_p_j_plus)) * np.min([1.5*delta_p_j_plus,1.5*delta_p_j_minus,(P_H_plus[j+1,0]-P_H_plus[j-1,0])/(2*dz)])  
    p_right_face_j_plus[j] = P_H_plus[j,0] + 0.5*p_limited_slope 
    p_left_face_j_plus[j] = P_H_plus[j,0] - 0.5*p_limited_slope 
    p_right_face_j_minus[j] = P_H_plus[j,0] - 0.5*p_limited_slope 
    p_left_face_j_minus[j] =P_H_plus[j-1,0] + 0.5*p_limited_slope
   
A_jplus = np.roll(A,1)
A_jplus[0] = A_jplus[1]
A_jminus = np.roll(A,-1)
A_jminus[-1] = A_jminus[-2]
    
#Mass equation
flux_mass_right_j_plus = rho_right_face_j_plus * u_right_face_j_plus
flux_mass_left_j_plus = rho_left_face_j_plus * u_left_face_j_plus
flux_mass_right_j_minus = rho_right_face_j_minus * u_right_face_j_minus
flux_mass_left_j_minus = rho_left_face_j_minus * u_left_face_j_minus
   
flux_mass_function_j_plus = (flux_mass_right_j_plus+flux_mass_left_j_plus)/2 - 0.45*(dz/dt)*(rho_right_face_j_plus - rho_left_face_j_plus)
flux_mass_function_j_minus = (flux_mass_right_j_minus+flux_mass_left_j_minus)/2 - 0.45*(dz/dt)*(rho_right_face_j_minus - rho_left_face_j_minus)

density_next =rho_H_plus[:,0] - dt*((A_jplus*flux_mass_function_j_plus - A_jminus*flux_mass_function_j_minus)/V) + dt*A*S_H_plus
  
#Momentum equation    
flux_right_j_plus = rho_right_face_j_plus * u_right_face_j_plus**2 
flux_left_j_plus = rho_left_face_j_plus * u_left_face_j_plus**2 
flux_right_j_minus = rho_right_face_j_minus * u_right_face_j_minus**2 
flux_left_j_minus = rho_left_face_j_minus * u_left_face_j_minus**2 

flux_function_j_plus = (flux_right_j_plus+flux_left_j_plus)/2 - 0.45*(dz/dt)*(rho_right_face_j_plus * u_right_face_j_plus - rho_left_face_j_plus * u_left_face_j_plus)
flux_function_j_minus = (flux_right_j_minus+flux_left_j_minus)/2 - 0.45*(dz/dt)*(rho_right_face_j_minus * u_right_face_j_minus - rho_left_face_j_minus * u_left_face_j_minus)
    
momentum_star = rho_H_plus[:,0]*u_H_plus[:,0] - dt*((np.roll(A,1)*flux_function_j_plus - np.roll(A,-1)*flux_function_j_minus)/V)+ dt*(-A*dPdr+ A*rho_H_plus[:,0]*((e_charge/m_H_plus)* E[:,1] - g) + A*u_H_plus[:,0]*S_H_plus[:])

momentum_next = momentum_star/(1+dt*(collision_freq(rho_H2,m_H_plus,m_H2,lambda_H2, e_charge)+ collision_freq(rho_He,m_H_plus,m_He,lambda_He, e_charge)))

#Energy equation
flux_energy_right_j_plus = 0.5 * A* rho_right_face_j_plus * u_right_face_j_plus**3 + (gamma/(gamma-1))*A*u_right_face_j_plus*p_right_face_j_plus
flux_energy_left_j_plus = 0.5 * A* rho_left_face_j_plus * u_left_face_j_plus**3 + (gamma/(gamma-1))*A*u_left_face_j_plus*p_left_face_j_plus
flux_energy_right_j_minus = 0.5 * A* rho_right_face_j_minus * u_right_face_j_minus**3  +(gamma/(gamma-1))*A*u_right_face_j_minus*p_right_face_j_minus
flux_energy_left_j_minus = 0.5 * A* rho_left_face_j_minus * u_left_face_j_minus**3 +(gamma/(gamma-1))*A*u_left_face_j_minus*p_left_face_j_minus

flux_energy_function_j_plus = (flux_energy_right_j_plus+flux_energy_left_j_plus)/2 - 0.45*(dz/dt)*((0.5*A*rho_right_face_j_plus * u_right_face_j_plus**2 +(1/(gamma-1))*A*p_right_face_j_plus) - (0.5*A*rho_left_face_j_plus * u_left_face_j_plus**2 + (1/(gamma-1))*A*p_left_face_j_plus))
flux_energy_function_j_minus = (flux_energy_right_j_minus+flux_energy_left_j_minus)/2 - 0.45*(dz/dt)*((0.5*A*rho_right_face_j_plus * u_right_face_j_plus**2 +(1/(gamma-1))*A*p_right_face_j_plus) - (0.5*A*rho_left_face_j_plus * u_left_face_j_plus**2 + (1/(gamma-1))*A*p_left_face_j_plus))
 
#energy_star = (0.5*A*rho_H_plus * u_H_plus**2 +(1/(gamma-1))*A*P_H_plus)- dt*((np.roll(A,1)*flux_energy_function_j_plus - np.roll(A,-1)*flux_energy_function_j_minus)/V)+ A*u_H_plus[:,0]*rho_H_plus[:,0]*((e_charge/m_H_plus)* E[:,1] - g) + 0.5*A*u_H_plus[:,0]**2*S_H_plus[:]

#pressure_star = (gamma-1)*(energy_star - 0.5*)

