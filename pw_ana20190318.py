# DISCRIPTION: Test analytical model of polar wind at Saturn from equations in Glocer+ 2007
# AUTHOR: C.J.Martin
# INSTITUTION: Lancaster University

# INPUTS: none other than the usual

# OUTPUT: Plots for now

# NOTES: two vectors for each variable are used, a long version used for 
# calculting the spatial steps and a short version '_s' used for calculating
# the final results for each iteration.
# Current inputs are from Moore+ 2008 for SATURN
# All in SI units unless explicitly commented

# HISTORY:
# 20190304 - Created
# 20190318 - Iterative Version


# ----------------Imports-------------------
import numpy as np
import matplotlib.pyplot as pl


#------------------functions----------------
#Function to calculate the reduced mass of two species SCHUNK + NAGY 2000
def red_mass(m_i,m_j): 
    return (m_i*m_j)/(m_i+m_j)
    
#Function to calculate the collision frequency between two species- SCHUNK + NAGY 2000
def collision_freq(rho_n,m_i,m_j,lambda_n, e_charge):
    return 2.21*np.pi*(rho_n/(m_i+m_j))*((lambda_n*e_charge**2)/\
                       red_mass(m_i,m_j))**0.5

#Function to calculate the momentum exhange rate for one species with one other
# species SCHUNK + NAGY 2000
def momentum_rate_1species(rho_n,m_i,m_j,lambda_n, e_charge,rho_i,u_i):
    return rho_i*collision_freq(rho_n,m_i,m_j,lambda_n, e_charge)*u_i

#Function to calculate the energy exchange rate for one ion with one neutral SCHUNK + NAGY 2000
def energy_rate_1species(rho_i,rho_n,m_i,m_j,lambda_n, e_charge,T_j,T_i,u_i,k_b):
    return ((rho_i * collision_freq(rho_n,m_i,m_j,lambda_n, e_charge))/\
            (m_i + m_j)) * (3* k_b * (T_j-T_i) + m_j*u_i**2)

#Function to calculate heat conductivity using assumptions in Banks+Kockarts 
#1973, Moore+ 2008 
def heat_conductivity(T,e_charge,m_i,m_p):
    return (4.6*10**4 *(m_i/m_p)**-0.5 * T**2.5)*e_charge*100 #J m-1 s-1 K-1

# from Raitt+ 1975: 7.7 x 10^5   new one is from Banks + Kockharts
def heat_conductivity_electrons(T,e_charge,gamma):
    return ((1.8*10**6)*(T**(5/2)))*e_charge*100 #J m-1 s-1 K-1

#Function to calculate plasma pressure
def plasma_pressure(n,k_b,T):
    return (n * k_b * T)

def plasma_temperature(n,k_b,P):
    return (P/(n*k_b))

#Function to calculate second term in electric field with 2 ionic species
def E_second_term(m_e,m_1,m_2,u_e,u_1,u_2,S_1,S_2,dMdt_1,dMdt_2):
    return (m_e/m_1 * ((u_e-u_1)*S_1-dMdt_1)) + (m_e/m_2 *\
           ((u_e-u_2)*S_2-dMdt_2))

#Function to calculate electric field
def E_parallel_short(e_charge, n_e, dFdt,A, dAdr, rho_e,u_e):
    return (-1/(e_charge*n_e) * (dFdt + (dAdr/A)*rho_e*u_e**2))

#Personalised plotting function by me for me
def plot_me_quick(x,y,xlabel,ylabel,grid):
    pl.plot(x,y)
    pl.ylabel(ylabel)
    pl.xlabel(xlabel)
    pl.grid(grid) 

def density_dt_ion(dt,A,S,rho,dFdt):
    return ((dt/A)*(A*S-dFdt)+rho)
 
def density_dt_electron(rho_1,rho_2,m_1,m_2,me):
    return ((rho_1/m_1 + rho_2/m_2)*m_e) #(rho_H_plus_dt/m_H_plus + rho_H3_plus_dt/m_H3_plus)
    #*m_e 

def velocity_dt_ion(dt,A,rho,rho_1,dFdr,dPdr,m_i,E,e_charge,g,dMdt,u,S):
    return ((dt/(A*rho_1))*(-dFdr - (A*dPdr) +(A*rho*((e_charge/m_i)*E - g)) + \
           A*dMdt + A*u*S)+((rho*u)/rho_1))

def velocity_dt_electron(n_e,n_1,n_2,u_1,u_2):
    return (1/n_e * (n_1*u_1 + n_2*u_2)) #this is where field aligned currents 
    #would go IF I HAD ANY
    
def pressure_dt_ion(dt,A,gamma,rho,rho_1,u,u_1,m_i, e_charge,E,g,dEdt,dMdt,P,dEngdr,dakTdr,S):    
    return (((dt/A)*(gamma-1))*(-dEngdr+A*rho*u*((e_charge/m_i)*E-g)+dakTdr+A*dEdt+\
     A*u*dMdt+0.5*A*u**2*S)+(0.5*rho*u**2 *(gamma-1))+P-(0.5*rho_1*u_1**2*(gamma-1)))
     
def temperature_dt_electron(dt,gamma,m_e,k_b,A,rho,u,T,S,dTedr,dAudr,dakTedr):
    return (dt*((gamma-1)*(m_e/(k_b*A*rho))*dakTedr-u*dTedr-(T/rho)*(S+((gamma-1)/A)*rho*dAudr))+T)

def electron_flux(n_i1,u_i1,n_i2,u_i2):
    return ((n_i1*u_i1)+(n_i2*u_i2))

def eV2vel(eV, m):
    return np.sqrt((eV*2*1.6*10**-19)/(m))

def vel2eV(vel, m):
    return (m*vel**2)/(2*1.6*10**-19)

def v2T(v,m):
    return (m*v**2)/(2*1.38*10**-23)

def T2v(T,m):
    return np.sqrt((2*1.38*10**-23*T)/m)

#--------------Start main-------------------
#debugging plots yes=1/no=0
dbug = 1
if dbug ==1:
    pl.figure(1)

#set up grid
z = np.linspace(1400000,61325000,800) #meters
x = np.linspace(0,799,800)
dz = 75000.0

#time steps
dt = 0.1 #seconds

#iterations
its = 10


# prefill iterative arrays
E = np.empty([len(z),its])
E[:,0] = np.nan # no initial values for electric field
e_flux = np.empty([len(z),its])
e_flux[:,0] = np.nan # no initial values for electron flux
H_plus_flux = np.empty([len(z),its])
H_plus_flux[:,0] = np.nan # no initial values for electron flux
H3_plus_flux = np.empty([len(z),its])
H3_plus_flux[:,0] = np.nan # no initial values for electron flux
rho_H_plus = np.empty([len(z),its])
rho_H3_plus = np.empty([len(z),its])
rho_e = np.empty([len(z),its])
n_H_plus = np.empty([len(z),its])
n_H3_plus = np.empty([len(z),its])
n_e = np.empty([len(z),its])
u_H_plus = np.empty([len(z),its])
u_H3_plus = np.empty([len(z),its])
u_e = np.empty([len(z),its])
P_H_plus = np.empty([len(z),its])
P_H3_plus = np.empty([len(z),its])
P_e = np.empty([len(z),its])
T_H_plus = np.empty([len(z),its])
T_H3_plus = np.empty([len(z),its])
T_e = np.empty([len(z),its])
kappa_H_plus = np.empty([len(z),its])
kappa_H3_plus = np.empty([len(z),its])
kappa_e = np.empty([len(z),its])


# Constants set-up
e_charge = 1.60217662*10**-19  #C - electron charge
m_p = 1.6726*10**-27 # kg - mass of a proton
m_e = 9.10938356 * 10**-31 # kg - mass of an electron
k_b = 1.38064852*10**-23 #m2 kg s-2 K-1  - boltzmann constant
lambda_H_plus = 1.66 #spcific heat ratio for H+ 
#lambda_H3_plus =   #spcific heat ratio for H3+ 
g = 10.44 #graviational acceleration
m_H2 = 2*m_p
m_He = 4*m_p
m_H_plus = m_p
m_H3_plus = 3*m_p
#Neutral gas polarizabilities
lambda_H2 = 0.82*10**-30 # m^3 from 0.82*10**-24 cm^3 Schunk&Nagy 2000
lambda_He = 0.21*10**-30 # m^3 from 0.21*10**-24 cm^3 Schunk&Nagy 2000
gamma = 5/3 #specific heat ratio



# cross sectional area A
alp= (1/(z[0]**3)) *0.00000001 
A = alp*(z**3)
if dbug == 1:
    pl.subplot(4,2,1)
    pl.plot(z/1000,A)
    pl.ylabel('Cross-sectional Area - A (m^2)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([0,0.001])
    pl.xlim([0,62000])
    pl.grid('on')
    
# -----------------------Initial Profiles--------------------------------------    
#Initial H+ temperature profile and hence pressure
#T_H_plus = 450*np.tanh(z_doub/10000000)
#T_H_plus = -450/(np.exp((z_doub/10000000))) +450
T_H_plus[:,0] = 25* (x/20)**2*np.exp(-0.1*(x/20)) +400
#T_H_plus[:,0] = v2T(u_H_plus[:,0],m_H_plus)


#Initial H3+ temperature profile and hence pressure
T_H3_plus[:,0] = 30* (x/15)**2*np.exp(-0.1*(x/15)) +400
#T_H3_plus[:,0] = v2T(u_H3_plus[:,0],m_H3_plus)


# initial H+ ion velocity - 1eV proton eV2vel(1,m_H_plus)
#u_H_plus[:,0] = np.ones(np.size(z))*eV2vel(0.0345,m_H_plus)
#u_H_plus[:,0] = 3.03 * x + eV2vel(0.0345,m_H_plus)
u_H_plus[:,0] = T2v(T_H_plus[:,0],m_H_plus)
if dbug==1:
    pl.subplot(4,2,2)
    line1,=pl.plot(z/1000,u_H_plus[:,0])
    pl.ylabel('Initial Velocity (m/s)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,1])
    pl.xlim([0,62000])
    pl.grid('on')
    
# initial H3+ ion velocity - 1eV ion
#u_H3_plus[:,0] = np.ones(np.size(z))*eV2vel(0.04,m_H3_plus)
#u_H3_plus[:,0] = 1.01 * x + eV2vel(0.04,m_H3_plus)
u_H3_plus[:,0] = T2v(T_H3_plus[:,0],m_H3_plus)
if dbug==1:
    pl.subplot(4,2,2)
    line2,=pl.plot(z/1000,u_H3_plus[:,0])
    pl.grid('on')
    

#initial H+ ion density - exponential decrease place holder
n_H_plus[:,0] = 2*10**10*np.exp(-0.5*(z/z[0])) + 6*10**5
rho_H_plus[:,0] = n_H_plus[:,0] * m_H_plus
if dbug==1:
    pl.subplot(4,2,3)
    n1,=pl.plot(z/1000,n_H_plus[:,0])
    pl.subplot(4,2,4)
    rho1,=pl.plot(z/1000,rho_H_plus[:,0])
    
#initial H3+ ion density - exponential decrease place holder
n_H3_plus[:,0] = 5*10**9*np.exp(-0.4*(z/z[0])) + 10**5
rho_H3_plus[:,0] = n_H3_plus[:,0] * m_H3_plus
if dbug==1:
    pl.subplot(4,2,3)
    n2,=pl.plot(z/1000,n_H3_plus[:,0])
    pl.ylabel('Initial Number Density (m^-3)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,10])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.subplot(4,2,4)
    rho2,=pl.plot(z/1000,rho_H3_plus[:,0])
    pl.ylabel('Initial Mass Density (m^-3)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([0,10])
    pl.xlim([0,62000])
    pl.grid('on') 
    
#initial electron density - quasi-neutrality
n_e[:,0] = n_H3_plus[:,0]+n_H_plus[:,0]
rho_e[:,0] = n_e[:,0] * m_e
if dbug==1:
    pl.subplot(4,2,3)
    n3,=pl.plot(z/1000,n_e[:,0])
    pl.subplot(4,2,4)
    rho3,=pl.plot(z/1000,rho_e[:,0])    
    
# initial electron velocity - calcualted from ion velocities and densities - quasinauetrality
u_e[:,0] = (1/n_e[:,0]) * (n_H3_plus[:,0] * u_H3_plus[:,0] + n_H_plus[:,0]*u_H_plus[:,0])
if dbug==1:
    pl.subplot(4,2,2)
    line3,=pl.plot(z/1000,u_e[:,0])
    pl.legend((line1, line2, line3), ('H+', 'H3+', 'e -'))
  
#initial neutral H2 density - CONSTANT
n_H2 = (10**10)*10**6*np.exp(-0.3*(z/z[0])) + 10000000
rho_H2 = n_H2 * m_H2
if dbug==1:
    pl.subplot(4,2,3)
    n4,=pl.plot(z/1000,n_H2)
    pl.subplot(4,2,4)
    rho4,=pl.plot(z/1000,rho_H2)
 
#initial neutral He density - CONSTANT
n_He = 10**5*10**6*np.exp(-0.5*(z/z[0])) + 10000000
rho_He = n_He * m_He
if dbug==1:
    pl.subplot(4,2,3)
    n5,=pl.plot(z/1000,n_He)
    pl.legend((n1,n2,n3,n4,n5),('H+','H3+','e -','H2','He'))
    pl.yscale('log')
    pl.subplot(4,2,4)
    rho5,=pl.plot(z/1000,rho_He)   
    pl.legend((rho1,rho2,rho3,rho4,rho5),('H+','H3+','e -','H2','He'))
    pl.yscale('log')
 
# H+ mass production rate    - CONSTANT
S_H_plus = 10**8 * m_H_plus *np.exp(-z/10000000) + 10**3* m_H3_plus
if dbug==1:
    pl.subplot(4,2,5)
    s1,=pl.plot(z/1000,S_H_plus)
    pl.ylabel('Mass Prodution Rate (kg m^-3 s^-1)')
    pl.xlabel('Distance Along Field Line (km)')
    #pl.ylim([10**-22,10**-17])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.yscale('log')

# H3+ mass production rate      - CONSTANT
S_H3_plus = 10**8 * m_H3_plus *np.exp(-z/10000000) + 10**3* m_H3_plus
S_e = (S_H3_plus/m_H3_plus + S_H_plus/m_H_plus) * m_e
if dbug==1:
    pl.subplot(4,2,5)
    s2,=pl.plot(z/1000,S_H3_plus)
    s3,=pl.plot(z/1000,S_e)
    pl.legend((s1,s2,s3),('H+','H3+','e -'))



P_H_plus[:,0] = plasma_pressure(n_H_plus[:,0],k_b,T_H_plus[:,0])
if dbug==1:
    pl.subplot(4,2,6)
    t1,=pl.plot(z/1000,T_H_plus[:,0])
    pl.ylabel('Temperature (K)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([0,2000])
    pl.xlim([0,62000])
    pl.grid('on')  
    pl.subplot(4,2,7)
    p1,=pl.plot(z/1000,P_H_plus[:,0])
    pl.ylabel('Pressure (N/m^2)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.ylim([10**-33,10**-4])
    pl.yscale('log')
    pl.xlim([0,62000])
    pl.grid('on')    
    
P_H3_plus[:,0] = plasma_pressure(n_H3_plus[:,0],k_b,T_H3_plus[:,0])
if dbug==1:
    pl.subplot(4,2,6)
    t2,=pl.plot(z/1000,T_H3_plus[:,0])
    pl.subplot(4,2,7)
    p2,=pl.plot(z/1000,P_H3_plus[:,0])
    
#Initial electron temperature profile and hence pressure
T_e[:,0] = v2T(u_e[:,0],m_e)
P_e[:,0] = plasma_pressure(n_e[:,0],k_b,T_e[:,0])
if dbug==1:
    pl.subplot(4,2,6)
    t3,=pl.plot(z/1000,T_e[:,0])
    pl.subplot(4,2,7)
    p3,=pl.plot(z/1000,P_e[:,0])
    
#Initial neutral temperature profile and hence pressure
T_neut = np.ones(np.size(x))* 400
P_neut = (n_H2+n_He) * k_b * T_neut
if dbug==1:
    pl.subplot(4,2,6)
    t4,=pl.plot(z/1000,T_neut)
    pl.legend((t1,t2,t3,t4),('H+','H3+','e-','n'))
    pl.subplot(4,2,7)
    p4,=pl.plot(z/1000,P_neut)
    pl.legend((p1,p2,p3,p4),('H+','H3+','e-','n'))    
    
# heat conductivities
kappa_H_plus[:,0] = heat_conductivity(T_H_plus[:,0],e_charge,m_H_plus,m_p)
kappa_H3_plus[:,0] = heat_conductivity(T_H3_plus[:,0],e_charge,m_H3_plus,m_p)
kappa_e[:,0] = heat_conductivity_electrons(T_e[:,0],e_charge,gamma)
if dbug==1:
    pl.subplot(4,2,8)
    k1,=pl.plot(z/1000,kappa_H_plus[:,0])
    k2,=pl.plot(z/1000,kappa_H3_plus[:,0])
    k3,=pl.plot(z/1000,kappa_e[:,0])
    pl.legend((k1,k2,k3),('H+','H3+','e -'))
    pl.ylabel('Heat Conductivity (J m-1 s-1 K-1)')
    pl.xlabel('Distance Along Field Line (km)')
    pl.xlim([0,62000])
    pl.grid('on')     
    pl.subplots_adjust(wspace=0.5, hspace=0.5)
  
d2Tdr=np.empty([len(z),])    
d2Tdr_H3=np.empty([len(z),]) 
d2Tdr_e=np.empty([len(z),])     
# ----------------------------------------------------------------------------   
for i in range(1,its):
    print(i)
    # calculate momentum exchange rate for H+
    dMdt_H_plus = - (momentum_rate_1species(rho_H2,m_H_plus,m_H2,lambda_H2, e_charge,rho_H_plus[:,i-1],u_H_plus[:,i-1]) + momentum_rate_1species(rho_He,m_H_plus,m_He,lambda_He, e_charge,rho_H_plus[:,i-1],u_H_plus[:,i-1]))
    # calculate momentum exchange rate for H3+
    dMdt_H3_plus = - (momentum_rate_1species(rho_H2,m_H3_plus,m_H2,lambda_H2, e_charge,rho_H3_plus[:,i-1],u_H3_plus[:,i-1]) + momentum_rate_1species(rho_He,m_H3_plus,m_He,lambda_He, e_charge,rho_H3_plus[:,i-1],u_H3_plus[:,i-1]))
    # calculate energy exchange rate for H+
    dEdt_H_plus = energy_rate_1species(rho_H_plus[:,i-1],rho_H2,m_H_plus,m_H2,lambda_H2, e_charge,T_neut,T_H_plus[:,i-1],u_H_plus[:,i-1],k_b) + energy_rate_1species(rho_H_plus[:,i-1],rho_He,m_H_plus,m_He,lambda_He, e_charge,T_neut,T_H_plus[:,i-1],u_H_plus[:,i-1],k_b)
    # calculate energy exchange rate for H3+
    dEdt_H3_plus = energy_rate_1species(rho_H3_plus[:,i-1],rho_H2,m_H3_plus,m_H2,lambda_H2, e_charge,T_neut,T_H3_plus[:,i-1],u_H3_plus[:,i-1],k_b) + energy_rate_1species(rho_H_plus[:,i-1],rho_He,m_H3_plus,m_He,lambda_He, e_charge,T_neut,T_H3_plus[:,i-1],u_H3_plus[:,i-1],k_b)
    
    #numerically calculate differentials- central difference
    dArhou = np.gradient(A * u_H_plus[:,i-1] * rho_H_plus[:,i-1],axis=0)/dz    
    dArhou_H3 = np.gradient(A * u_H3_plus[:,i-1] * rho_H3_plus[:,i-1],axis=0)/dz
    dAudr = np.gradient(A*u_e[:,i-1],axis=0)/dz
    dArhou2= np.gradient(A * u_H_plus[:,i-1]**2 * rho_H_plus[:,i-1],axis=0)/dz
    dArhou2_H3= np.gradient(A * u_H3_plus[:,i-1]**2 * rho_H3_plus[:,i-1],axis=0)/dz
    dPdr = np.gradient(P_H_plus[:,i-1],axis=0)/dz
    dPdr_H3 = np.gradient(P_H3_plus[:,i-1],axis=0)/dz
    dTdr = np.gradient(T_H_plus[:,i-1],axis=0)/dz
    dTdr_H3 = np.gradient(T_H3_plus[:,i-1],axis=0)/dz
    dTedr = np.gradient(T_e[:,i-1],axis=0)/dz
    dAdr = np.gradient(A,axis=0)/dz
    dPrhou2 = np.gradient(P_e[:,i-1]+rho_e[:,i-1]*u_e[:,i-1]**2,axis=0)/dz
    dEngdr= np.gradient((0.5 * A * u_H_plus[:,i-1]**3 * rho_H_plus[:,i-1] - gamma/(gamma-1) * A * u_H_plus[:,i-1] * P_H_plus[:,i-1]),axis=0)/dz
    dEngdr_H3= np.gradient((0.5 * A * u_H3_plus[:,i-1]**3 * rho_H3_plus[:,i-1] - gamma/(gamma-1) * A * u_H3_plus[:,i-1] * P_H3_plus[:,i-1]),axis=0)/dz
    dEdr = np.gradient(E_second_term(m_e,m_H_plus,m_H3_plus,u_e[:,i-1],u_H_plus[:,i-1],u_H3_plus[:,i-1],S_H_plus,S_H3_plus,dMdt_H_plus,dMdt_H3_plus),axis=0)/dz
    dkdr = np.gradient(kappa_H_plus[:,i-1],axis=0)/dz
    dkdr_H3 = np.gradient(kappa_H3_plus[:,i-1],axis=0)/dz    
    dkdr_e = np.gradient(kappa_e[:,i-1],axis=0)/dz   
    
    #aproximation for second order differential: central, forward and backward euler
    for l in range(1,len(z)-1):
        d2Tdr[l] =  (T_H_plus[l+1,i-1] - 2*T_H_plus[l,i-1] + T_H_plus[l-1,i-1])/(dz**2)
        d2Tdr_H3[l] =  (T_H3_plus[l+1,i-1] - 2*T_H3_plus[l,i-1] + T_H3_plus[l-1,i-1])/(dz**2)
        d2Tdr_e[l] =  (T_e[l+1,i-1] - 2*T_e[l,i-1] + T_e[l-1,i-1])/(dz**2)
    d2Tdr[0] = (T_H_plus[2,i-1] - 2*T_H_plus[1,i-1] + T_H_plus[0,i-1])/(dz**2)
    d2Tdr[-1] = (T_H_plus[-1,i-1] - 2*T_H_plus[-2,i-1] + T_H_plus[-3,i-1])/(dz**2)
    d2Tdr_H3[0] = (T_H3_plus[2,i-1] - 2*T_H3_plus[1,i-1] + T_H3_plus[0,i-1])/(dz**2)
    d2Tdr_H3[-1] = (T_H3_plus[-1,i-1] - 2*T_H3_plus[-2,i-1] + T_H3_plus[-3,i-1])/(dz**2)
    d2Tdr_e[0] = (T_e[2,i-1] - 2*T_e[1,i-1] + T_e[0,i-1])/(dz**2)
    d2Tdr_e[-1] = (T_e[-1,i-1] - 2*T_e[-2,i-1] + T_e[-3,i-1])/(dz**2)
    
    dakTdr = (A * kappa_H_plus[:,i-1] * d2Tdr) + (A * dkdr * dTdr) + (dAdr * kappa_H_plus[:,i-1]* dTdr)
    dakTdr_H3 = (A * kappa_H3_plus[:,i-1] * d2Tdr_H3) + (A * dkdr_H3 * dTdr_H3) + (dAdr * kappa_H3_plus[:,i-1]* dTdr_H3)
    dakTedr = (A * kappa_e[:,i-1] * d2Tdr_e) + (A * dkdr_e * dTedr) + (dAdr * kappa_e[:,i-1]* dTedr)
    
    #parallel electric field - initial (short assumption)
    E[:,i] = E_parallel_short(e_charge, n_e[:,i-1].T, dPrhou2, A.T, dAdr, rho_e[:,i-1].T, u_e[:,i-1].T) + 1/(e_charge*n_e[:,i-1]) * dEdr.T
    
    # updated values for next step
    #Mass conservation equation
    rho_H_plus[:,i] = density_dt_ion(dt,A,S_H_plus,rho_H_plus[:,i-1],dArhou.T)
    rho_H3_plus[:,i] = density_dt_ion(dt,A,S_H3_plus,rho_H3_plus[:,i-1],dArhou_H3.T)
    rho_e[:,i] = density_dt_electron(rho_H_plus[:,i],rho_H3_plus[:,i],m_H_plus,m_H3_plus,m_e)
    n_H_plus[:,i] = rho_H_plus[:,i] / m_H_plus
    n_H3_plus[:,i] = rho_H3_plus[:,i] / m_H3_plus
    n_e[:,i] = rho_e[:,i] / m_e
    
    #Momentum conservation equation
    u_H_plus[:,i] = velocity_dt_ion(dt,A,rho_H_plus[:,i-1],rho_H_plus[:,i],dArhou2.T,dPdr.T,m_H_plus,E[:,i],e_charge,g,dMdt_H_plus,u_H_plus[:,i-1],S_H_plus)
    u_H3_plus[:,i] = velocity_dt_ion(dt,A,rho_H3_plus[:,i-1],rho_H3_plus[:,i],dArhou2_H3.T,dPdr_H3.T,m_H3_plus,E[:,i],e_charge,g,dMdt_H3_plus,u_H3_plus[:,i-1],S_H3_plus)
    u_e[:,i] = velocity_dt_electron(n_e[:,i],n_H_plus[:,i],n_H3_plus[:,i],u_H_plus[:,i],u_H3_plus[:,i])
    
    #Energy conservation equation
    P_H_plus[:,i] = pressure_dt_ion(dt,A,gamma,rho_H_plus[:,i-1],rho_H_plus[:,i],u_H_plus[:,i-1],u_H_plus[:,i],m_H_plus, e_charge,E[:,i],g,dEdt_H_plus,dMdt_H_plus,P_H_plus[:,i-1],dEngdr.T, dakTdr.T,S_H_plus) 
    T_H_plus[:,i] = plasma_temperature(n_H_plus[:,i],k_b,P_H_plus[:,i])
    P_H3_plus[:,i] = pressure_dt_ion(dt,A,gamma,rho_H3_plus[:,i-1],rho_H3_plus[:,i],u_H3_plus[:,i-1],u_H3_plus[:,i],m_H3_plus, e_charge,E[:,i],g,dEdt_H3_plus,dMdt_H3_plus,P_H3_plus[:,i-1],dEngdr_H3.T,dakTdr_H3.T,S_H3_plus) 
    T_H3_plus[:,i] = plasma_temperature(n_H3_plus[:,i],k_b,P_H3_plus[:,i])
    T_e[:,i] =  temperature_dt_electron(dt,gamma,m_e,k_b,A,rho_e[:,i-1],u_e[:,i-1],T_e[:,i-1],S_e,dTedr.T,dAudr.T,dakTedr.T)
    
#    pl.figure(2)
#    plot_me_quick(z/1000,T_e[:,1],'Distance Along Field Line (km)','Temp','on')
##    plot_me_quick(z/1000,dt*(gamma-1)*m_e/(kappa_e[:,0]*A*rho_e[:,0]) * dakTedr.T,'Distance Along Field Line (km)','Temp','on')
##    plot_me_quick(z/1000,-dt*u_e[:,0]*dTedr.T,'Distance Along Field Line (km)','Temp','on')
#    plot_me_quick(z/1000,-dt*(T_e[:,0]/rho_e[:,0])*S_e,'Distance Along Field Line (km)','Temp','on')
##    plot_me_quick(z/1000,-dt*(T_e[:,0]/rho_e[:,0])*(((gamma-1)/A)* rho_e[:,0]*dAudr.T),'Distance Along Field Line (km)','Temp','on')
#    
    P_e[:,i] = plasma_pressure(rho_e[:,i]/m_e, k_b,T_e[:,i])
    
    #Heat conductivities
    kappa_H_plus[:,i] = heat_conductivity(T_H_plus[:,i],e_charge,m_H_plus,m_p)
    kappa_H3_plus[:,i] = heat_conductivity(T_H3_plus[:,i],e_charge,m_H3_plus,m_p)
    kappa_e[:,i] = heat_conductivity_electrons((T_e[:,i]),e_charge,gamma)
    
    #Electron Flux
    e_flux[:,i] = electron_flux(n_H_plus[:,i],u_H_plus[:,i],n_H3_plus[:,i],u_H3_plus[:,i])*A
    H_plus_flux[:,i] = n_H_plus[:,i]*u_H_plus[:,i] * A
    H3_plus_flux[:,i] = n_H3_plus[:,i]*u_H3_plus[:,i] *A
    
# results figure
pl.figure(2)
pl.subplot(3,2,1)
plot_me_quick(z/1000,E,'Distance Along Field Line (km)','E Parallel (V/m)','on')
pl.subplot(3,2,2)
plot_me_quick(z/1000,n_e,'Distance Along Field Line (km)','Number Density (/m^3)','on')
plot_me_quick(z/1000,n_H_plus,'Distance Along Field Line (km)','Number Density (/m^3)','on')
plot_me_quick(z/1000,n_H3_plus,'Distance Along Field Line (km)','Number Density (/m^3)','on')
pl.subplot(3,2,3)
plot_me_quick(z/1000,T_H_plus,'Distance Along Field Line (km)','Temperature (K)','on')
plot_me_quick(z/1000,T_H3_plus,'Distance Along Field Line (km)','Temperature (K)','on')
pl.subplot(3,2,4)
plot_me_quick(z/1000,e_flux,'Distance Along Field Line (km)','Electron Flux (m^-2 s^-1) x A (m^2) ','on')
pl.subplot(3,2,5)
plot_me_quick(z/1000,H_plus_flux,'Distance Along Field Line (km)','H+ Flux (m^-2 s^-1)x A (m^2)','on')
pl.subplot(3,2,6)
plot_me_quick(z/1000,H3_plus_flux,'Distance Along Field Line (km)','H3+ Flux (m^-2 s^-1)x A (m^2)','on')

# H+ plotting
pl.figure(3)
pl.subplot(2,2,1)
plot_me_quick(z/1000,rho_H_plus,'Distance Along Field Line (km)','Density (kg/m^3)','on')

pl.subplot(2,2,2)
plot_me_quick(z/1000,u_H_plus,'Distance Along Field Line (km)','Velocity (m/s)','on')

pl.subplot(2,2,3)
plot_me_quick(z/1000,P_H_plus,'Distance Along Field Line (km)','Pressure (N/m^2)','on')

pl.subplot(2,2,4)
plot_me_quick(z/1000,T_H_plus,'Distance Along Field Line (km)','Temperature (K)','on')

# H3+
pl.figure(4)
pl.subplot(2,2,1)
plot_me_quick(z/1000,rho_H3_plus,'Distance Along Field Line (km)','Density (kg/m^3)','on')

pl.subplot(2,2,2)
plot_me_quick(z/1000,u_H3_plus,'Distance Along Field Line (km)','Velocity (m/s)','on')

pl.subplot(2,2,3)
plot_me_quick(z/1000,P_H3_plus,'Distance Along Field Line (km)','Pressure (N/m^2)','on')

pl.subplot(2,2,4)
plot_me_quick(z/1000,T_H3_plus,'Distance Along Field Line (km)','Temperature (K)','on')

# e-
pl.figure(5)
pl.subplot(2,2,1)
plot_me_quick(z/1000,rho_e,'Distance Along Field Line (km)','Density (kg/m^3)','on')

pl.subplot(2,2,2)
plot_me_quick(z/1000,u_e,'Distance Along Field Line (km)','Velocity (m/s)','on')

pl.subplot(2,2,3)
plot_me_quick(z/1000,P_e,'Distance Along Field Line (km)','Pressure (N/m^2)','on')

pl.subplot(2,2,4)
plot_me_quick(z/1000,T_e,'Distance Along Field Line (km)','Temperature (K)','on')