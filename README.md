# DESCRIPTION: 
# Numerical model of polar wind at Saturn/Jupiter 
# use current version: pw_euler.py

# AUTHOR: 
# C.J.Martin

# INSTITUTION: 
# Lancaster University

# INPUTS/IMPORTS: 
# dipolefield.py, pw.py, pw_plotting_tools.py and planet.py modules required
# numpy and matplotlib

# EXAMPLE USEAGE:
# For one dimensinaly model along one specified field line:
# import pw_euler
# pw_euler.oneD_field_line('jupiter',0.01,10000,3,16,0,0,1,0,'anything')
#  				(planet_name,dt,its,rs,lshell,FAC_flag,CF_flag,plots,saves,run_name) 
# For a range of ionospheric latitude built into model giving equatorial flux rates:
# import pw_euler
# pw_euler.equatorial_flux('jupiter',0.01,10000,3,0,0,1,0,'anything')
#                   (planet_name,dt,its,rs,FAC_flag,CF_flag,plots,saves,run_name)
# NOTES:  functions do not take the same imputs (no lshell for equatorial_flux)
#         planet_name = string with planet name ('saturn' or 'jupiter')
#         dt = time step (0.01)
#         its = iterations (100)
#		  rs = approximate rs extent of model
#		  lshell = lshell of field line (oneD_field_line only)
#         FAC_flag = flag for field aligned currents (0=no change, 1=removed)
#         CF_flag = flag for centrifugal force (0=no change, 1=removed)
#         plots = do you want plotting (1=yes, 0=no)
#         saves = do you want to save the output to file (1=yes, 0=no)
#		  run_name = identifiable feature of this run to save to

# OUTPUT: 
# Plots on screen/in cmdl depending on user
# Plots save to specified folder
# Data saves to specified folder in following format (oneD_field_line only):
#	-------SETUP------- 
#	Planet=saturn
#	Time Step=0.01
#	Spatial Step=75000.0
#	Iterations=100
#	Outer limit (radii)=3
#	L-Shell=33
#	Field Aligned Currents removed:1,included:0 =0
#	Centrifugal Stress  removed:1,included:0 =0
#	Number of Ionic species=2
#	Number of Neutral species=4
#	Ion Species 1: H plus
#	Ion Species 2: H3 plus
#	Neutral Species 1: H2
#	Neutral Species 2: He
#	Neutral Species 3: H
#	Neutral Species 4: H2O
#	-------VECTORS------- 
#	Spatial Grid: 
#	Gravitational Acceleration: 
#	Centrifugal Acceleration: 
#	Electric Field: 
#	-------ELECTRONS------- 
#	rho (kg/m^3): 
#	n (/m^3): 
#	u (m/s): 
#	P (Pa): 
#	T (K): 
#	kappa:
#	-------ION_NAME------- 
#	rho (kg/m^3): 
#	n (/m^3): 
#	u (m/s): 
#	P (Pa): 
#	T (K): 
#	kappa: 

# GENERAL NOTES: 
# All in SI units unless explicitly commented otherwise

# DICTIONARIES:
# planet.py returns three dictionaries:
# ions - contains all ion information for as many ions as are included in the input file (dictionary
#		 inside a dictionary, ion species indexed with numbers)
#		 e.g. : "name":"H plus", #name of particle
#               "mass":m_H_plus, #mass of species
#            	"T": T_H_plus, #temperature
#            	"u": u_H_plus,  #velocity
#            	"n": n_H_plus, #number density
#            	"rho": rho_H_plus, #mass density
#            	"P": P_H_plus, #pressure
#            	"S": S_H_plus, #mass production rate
#            	"kappa":kappa_H_plus #heat conductivity
# neutrals- contains all neutral information for as many neutrals as are included in the input file (dictionary
#		    inside a dictionary, neutral species indexed with numbers)
# 		    e.g. :"name":"H2", #name of particle
#                 "mass":m_H2, #mass of species
#            	  "T": T_neut, #temperature
#            	  "u": u_neut, #velocity
#            	  "n": n_H2, #number density
#            	  "rho": rho_H2, #mass density
#            	  "P":P_neut, #pressure
#            	  "lambda":lambda_H2 #polarisability
#electrons -contains all electron information
#		 e.g. : "mass":m_e, #mass of species
#            	"T": T_e, #temperature
#            	"u": u_e,  #velocity
#            	"n": n_e, #number density
#            	"rho": rho_e, #mass density
#            	"P": P_e, #pressure
#            	"S": S_e, #mass production rate
#            	"kappa":kappa_e #heat conductivity
#
# VARIABLES:
# _neut - neutral species
# _ion - ion species (i.e. H_plus)
# _e - electron

# Name [Size] (Units) Description 
# A [spacial bins,] (m^-3) Cross-section of flux tube increasing with distance
# E [spacial bins, iterations] (V/m) Electric field at each iteration
# P_X [spacial bins, iterations] (N/m^2) Pressure of ion species/electron
# S_X [spacial bins,] (kg/m^3s) Particle mass production rate
# T_X [spacial bins, iterations] (K) Particle temperature
# ac [spacial bins,] (m/s^2) centrifugal acceleration
# ag [spacial bins,] (m/s^2) gravitational acceleration
# dAdr [spacial bins,] Spacial derivative of cross sectional area
# dArhou [spacial bins,] Spacial derivative of cross sectional area*density*velocity
# dArhou2 [spacial bins,] Spacial derivative of cross sectional area*density*velocity^2
# dAudr [spacial bins,] Spacial derivative of cross sectional area*velocity
# dEdr [spacial bins,] Spacial derivative of second term in electric field equation (Glocer+ 2007)
# dEdt_X [spacial bins,] Energy exchange rate 
# dEngdr [spacial bins,] Spacial derivative of second term of energy equation in Glocer+ 2007
# dkdr [spacial bins,] spacial derivative of heat conductivity
# dMdt_X [spacial bins,] Momentum exchange rate
# dPdr [spacial bins,] Spacial derivative of pressure
# dPrhou2 [spacial bins,] Spacial derivative of first term in electric field equation
# dTdr [spacial bins,] Spacial derivative of temperature
# dakTdr [spacial bins,] spacial derivative of kinteic energy term with differential of temperature
# e_flux [spacial bins, iterations] (/ms)electron flux
# FAC [spacial bins,] (A/m^2) field aligned currents (sometimes named cd)
# ghosts [4,2] ghost points [gb_z,ge_z,gb_x,ge_x] b- beginning, e-end, z-on z array, x- on x array
# ion#_flux [spacial bin,its] (/ms) ion fluxes
# kappa_X [spacial bins, iterations] (J/msK) heat conductivity
# n_X [spacial bins, iterations] (m^3) number density 
# phi [spacial bins,] (rad) angle between dipole field line and equator
# rho_X [spacial bins, iterations] (km/m^3) mass density 
# u_X [spacial bins, iterations] (m/s) velocity
# x [spacial bins,] vector for forming initial condition functions
# z [spacial bins,] (m) vector of distances from 1 bar level

# CONSTANTS:
# alp [1] (na) constant used to normalise cross-section A
# b0 [1] (T) magnetic field at surface or 1 bar
# dbug [1] debugging plotting option
# dipole_offset [1] offset of dipole from rotational pole
# dt [1] (s) time step
# dz [1] (m) spacial step
# consts [6] vector of various constants [radius, mass_planet,b0,rot_period,dipole_offset,g]
# e_charge [1] electron charge
# g [1] (m/s^2) gravitational acceleration
# gamma [1] adiabatic constant
# its [1] number of iterations/time steps
# k_b [1] boltzmann constant
# lambda_X [1] neutral gas polarisation or specific heat ratio
# lshell [1]  rough lshell of field line
# mass_planet [1] (kg) mass of planet
# m_X [1] (kg) mass
# num_ionic_species [1] number of ionic species (read from dictionary ions)
# num_neutral_species [1] number of neutral species (read from dictionary neutrals)
# radius [1] (m) planetary radius
# rot_period [1] rotation period of planet

# FUNCTIONS:
# What goes in comes out the same shape
# all functions described in pw.py and pw_plotting_tools.py modules in each function

# MODULES:
# pw.py - all non plotting functions
# pw_plotting_tools.py - all plotting functions
# dipolar_field.py - determining centrifugal accel and grav from dipole field arrangement (c.f. Dave Constable)
# planet.py - choice of Jupiter or Saturn input values output as dictionaries

# REFERENCES:
# Glocer+ 2007
# Moore+ 2008
# Moore+ 2004
# Schunk & Nagy 2000
# Banks & Kockarts
